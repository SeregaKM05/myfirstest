import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

public class FirstTest {
    @Test
    public void myTest(){
        WebDriver driver= new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("https://ithillel.ua/");
        driver.findElement(By.xpath("//*[@id=\"body\"]/div[1]/div[1]/div[1]/div/nav/a[3]")).click();
        driver.findElement(By.xpath("//*[@id=\"body\"]/div[1]/div[1]/div[2]/div/div/nav/ul/li[2]/button")).click();
        driver.findElement(By.xpath("//*[@id=\"coursesMenuControlPanel\"]/ul/li[3]/button")).click();
        driver.findElement(By.xpath("//*[@id=\"block-202048\"]/div/ul/li[2]/a")).click();
        assertTrue(driver.findElement(By.xpath("//*[@id=\"body\"]/div[1]/main/section[1]/div[3]/div/div/div[1]/div/h1/span[1]/strong")).isDisplayed(),"Error-- not FIND");
        driver.close();
    }
    @Test
    public void myTest1() {
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("https://www.google.com.ua/");
        driver.manage().window().maximize();
        driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input")).sendKeys("Prom", Keys.ENTER);
        driver.findElement(By.xpath("//*[@id=\"tads\"]/div/div/div/div[1]/a/div[1]/span")).click();
        driver.findElement(By.xpath("//*[@id=\"page-block\"]/div/header/div/div/div/div/div[2]/div/div/div[1]/div/div/form/div/div[1]/input")).sendKeys("Redmi note 8 pro", Keys.ENTER);
        driver.navigate().back();
        assertTrue(driver.findElement(By.name("search_term")).isDisplayed());
        driver.close();
    }
    @Test
    public void login() {

        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://global.bittrex.com/account/login");
        WebElement username = driver.findElement(By.name("UserName"));
        WebElement password = driver.findElement(By.id("Password"));
        WebElement login = driver.findElement(By.xpath("//*[@id=\"loginForm\"]/div[4]/button"));
        username.sendKeys("pumba@gmail.com");
        password.sendKeys("akulamatata");
        login.click();
        WebElement danger = driver.findElement(By.xpath("//*[@id=\"loginForm\"]/div[1]"));
        assertTrue(danger.isDisplayed());
        driver.close();
    }
}
